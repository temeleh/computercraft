local function forward(step) for a = 1, step do turtle.forward() end end
local function back(step) for a = 1, step do turtle.back() end end
local function down(step) for a = 1, step do turtle.digDown() turtle.down() end end
local function turn180() turtle.turnLeft() turtle.turnLeft() end
local function dig() while turtle.inspect() do turtle.dig() end end

local args = {...}

local x = tonumber(args[1])
local y = tonumber(args[2])
local passes = args[3]


local function processCoord(i, j) 
    if j == 1 and i == 1 then 
        down(3)
    elseif j ~= 1 then
        turtle.forward() 
    end

    turtle.digUp()
    if j < y then dig() end
    turtle.digDown()
end

local function putItemsInChest() 
    for i = 1, 16 do
        turtle.select(i)
        turtle.drop()
    end
    turtle.select(1) 
end

local function passParityHandling(pass, i, func1, func2)
    if pass % 2 == 0 then 
        if i % 2 ~= 0 then func1() else func2() end
    else
        if i % 2 == 0 then func1() else func2() end
    end
end

local function gotoChestAndReturn(i, j, pass)
    --up
    for a = 1, (pass * 3) do turtle.up() end

    passParityHandling(pass, i, turtle.turnRight, turtle.turnLeft)

    local retJ = j - 1
    local retI = i - 1
    
    if pass % 2 == 0 then if i % 2 ~= 0 then retJ = y - j end
    elseif i % 2 == 0 then retJ = y - j end
    
    if pass % 2 == 0 then retI = x - i end

    --towards chest xy
    forward(retI)
    turtle.turnLeft()
    forward(retJ)

    --items into chest
    putItemsInChest()

    --from chest xy
    back(retJ)
    turtle.turnRight()
    back(retI)

    
    passParityHandling(pass, i, turtle.turnLeft, turtle.turnRight)

    --down
    for a = 1, (pass * 3) do turtle.down() end
end

local function makePass(pass)
    for i = 1, x do
        for j = 1, y do
            processCoord(i, j)

            if turtle.getItemCount(16) > 0 then
                gotoChestAndReturn(i, j, pass)
            end
        end
    
        if i < x then
            if i % 2 == 0 then turtle.turnLeft() else turtle.turnRight() end
            dig()
            turtle.forward()
            if i % 2 == 0 then turtle.turnLeft() else turtle.turnRight() end
        end
    end
    turn180()
end



for pass = 1, passes do
    makePass(pass) 

    if x % 2 == 0 then 
        forward(y - 1)
        turn180()
    end
end