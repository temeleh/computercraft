local function craft()
    while turtle.getItemCount(1) > 0 do
        turtle.placeDown()
    
        if turtle.getItemCount(2) == 0 then
            turtle.dropUp()
        else
            turtle.select(2)
            turtle.dropUp()
            turtle.select(1)
        end
    end
end

craft()

while true do
    turtle.select(1)
    os.pullEvent("redstone")

    while turtle.suck() do
        craft()
    end
end