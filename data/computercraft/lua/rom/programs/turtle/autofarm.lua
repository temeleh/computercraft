local function dropAll()
    for i = 2, 16 do
        turtle.select(i)

        if turtle.compareTo(1) then
            turtle.dropDown()
        else
            turtle.dropUp()
        end
    end
    turtle.select(1)
end

while true do
    local success, data = turtle.inspect()
    
    if success then
        if data.state.age == 7 then turtle.dig() end
        
        if turtle.getItemCount(16) > 0 then
            dropAll()
        end
    else 
        turtle.place()
    end    
end
