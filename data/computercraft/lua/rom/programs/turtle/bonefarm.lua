local function movement(step, func) 
    local c = 1
    while c <= step do
        if func() then c = c + 1 end
    end
end

local function forward(step) movement(step, turtle.forward) end
local function back(step) movement(step, turtle.back) end
local function down(step) movement(step, turtle.down) end
local function up(step) movement(step, turtle.up) end

local function turn180() turtle.turnLeft() turtle.turnLeft() end

local function inspectUpName(name) success, data = turtle.inspectUp() return data.name == name end
local function inspectDownName(name) success, data = turtle.inspectDown() return data.name == name end
local function inspectName(name) success, data = turtle.inspect() return data.name == name end
local function inspectNameColor(name, color) success, data = turtle.inspect() return data.name == name and (data.state ~= nil and data.state.color == color) end

local function hasNeighborName(front, behind, left, right, name) 
    return (front.name == name) or (behind.name == name) or (left.name == name) or (right.name == name)
end
local function hasNeighborColor(front, behind, left, right, color) 
    return (front.state ~= nil and front.state.color == color) or (behind.state ~= nil and behind.state.color == color) or (left.state ~= nil and left.state.color == color) or (right.state ~= nil and right.state.color == color)
end



local args = {...}

if args[1] == nil then 
    print("Usage:\nbonefarm <levelOffset> <levelSkip>\nTurtle's first slot must have seeds!")
    return
end

local size = 18
local levels = 6
local noItemSlot = 16
local noItemSlotResume = 14

local levelOffset = 0
local levelSkip = 0
local checkMoreSeeds = true

if args[1] ~= nil then levelOffset = tonumber(args[1]) end
if args[2] ~= nil then levelSkip = tonumber(args[2]) end


local slab = "minecraft:stone_slab"
local stainedGlass = "minecraft:stained_glass"
local stone = "minecraft:stone"
local chest = "minecraft:chest"
local plant = "cropmod:bone_plant"
local lantern = "minecraft:lit_pumpkin"
local farmland = "minecraft:farmland"
local interface = "appliedenergistics2:interface"
local smoothAEBlock = "appliedenergistics2:smooth_sky_stone_block"





local function putItemsInChest() 
    for i = 2, 16 do
        turtle.select(i)
        turtle.drop()
    end
    turtle.select(1) 
end

local function refuel()
    if turtle.getFuelLevel() < 10000 then
        turtle.turnRight()
        turtle.select(2)
        while turtle.getFuelLevel() <= turtle.getFuelLimit() - 640 do
            if turtle.getItemCount() == 0 then
                turtle.suck()

                if turtle.getFuelLevel() > 8000 and turtle.getItemCount() == 0 then break end
            end
            turtle.refuel(1) --change to refuel(1) when refueling coal
        end
        turtle.drop()
        turtle.select(1)
        turtle.turnLeft()
    end
end

-- Stops if coal count in ae system is > 8000. (Back redstone signal) 
-- Resumes after waiting for an hour, if count < 8000
local function stopWorkIfPossible()
    while rs.getInput("back") do
        os.sleep(3600)
    end
end




local function gotoChest(startAlign, endAlign)
    
    if startAlign == nil then startAlign = true end
    if endAlign == nil then endAlign = true end
    
    if startAlign or startAlign == "partly" then
        if startAlign ~= "partly" then 
            turtle.turnLeft()
        end

        forward(1)
        turtle.turnRight()
        forward(1)
       
    end

    while not inspectDownName(stone) do
        down(1)
    end

    if endAlign then
        back(1)
        turtle.turnLeft()
        back(1)
        turtle.turnRight()
    end

    putItemsInChest()
    refuel()

    stopWorkIfPossible()
end


local function handleFullInventory(i, j, level)
    if j ~= 1 then if i % 2 == 0 then turtle.turnRight() else turtle.turnLeft() end end

    local retJ = j - 1
    local retI = i - 1
    
    if j ~= 1 then if i % 2 == 0 then retJ = size - retJ end end

    --towards chest xy
    forward(retI)
    turtle.turnLeft()
    forward(retJ)

    --items into chest
    gotoChest("partly")

    up(level * 4)

    --from chest xy
    turtle.turnLeft() 
    back(retJ)
    turtle.turnRight()    
    back(retI)

    if j ~= 1 then if i % 2 ~= 0 then turtle.turnRight() else turtle.turnLeft() end end
end

local function plantSeed()
    local success, data = turtle.inspectDown()

    if success then 
        if data.name == plant and data.state.age == 7 then
            turtle.digDown()
            turtle.placeDown()
            checkMoreSeeds = true
        end
    elseif turtle.getItemCount(1) > 2 then
        turtle.placeDown()
    elseif checkMoreSeeds then
        for i = 2, 16 do
            if turtle.compareTo(i) then
                turtle.select(i) 
                turtle.transferTo(1)
                
                turtle.select(1)
                turtle.placeDown()
                break
            end
        end
        checkMoreSeeds = false
    end
end

local function processCoord(i, j, level) 
    if j > 2 or (j == 1 and i ~= size) then
        forward(1) 
    end

    plantSeed()

    if turtle.getItemCount(noItemSlot) > 0 or turtle.getFuelLevel() < 1000 then
        handleFullInventory(i, j, level)
    end
end

local function harvestLevel(level)
    for i = 1, size do
        for j = 2, size do
            processCoord(i, j, level)
        end
    
        if i < size then
            if i % 2 == 0 then turtle.turnLeft() else turtle.turnRight() end
            forward(1)
            if i % 2 == 0 then turtle.turnLeft() else turtle.turnRight() end
        end
    end

    forward(1)
    turtle.turnRight()

    for i = size, 2, -1 do
        processCoord(i, 1, level)
    end

    forward(1)
end



local function main()
    up(levelOffset * 4)

    for level = (1 + levelOffset), levels, 1 + levelSkip do
        up(4)
        if level > 1 + levelOffset then up(levelSkip * 4) end
        turtle.turnRight()
        forward(1)
    
        harvestLevel(level) 
    
    end
    
    gotoChest()
end




















local function getNeighbors()
    local ups, upd = turtle.inspectUp()
    local dws, dwd = turtle.inspectDown()
    local s, d = turtle.inspect()

    turtle.turnLeft()
    local ls, ld = turtle.inspect()

    turtle.turnLeft()
    local bs, bd = turtle.inspect()

    turtle.turnLeft()
    local rs, rd = turtle.inspect()

    turtle.turnLeft()
    return d, upd, dwd, ld, rd, bd
end

local function handlePlantResume(right)
    if right.name == stainedGlass then turn180() end

    while not inspectNameColor(stainedGlass, "gray") do
        if turtle.getItemCount(noItemSlotResume) == 0 then plantSeed() end

        if inspectName(stainedGlass) then turtle.turnRight() forward(1)
        else forward(1) end
    end

    gotoChest()
end


local function resume()
    turtle.select(1)

    local front, top, under, left, right, behind = getNeighbors()

    --near chest
    if front.name == chest and right.name == interface then gotoChest(false, false) return end
    if front.name == interface and left.name == chest then turtle.turnLeft() gotoChest(false, false) return end
    if left.name == interface and behind.name == chest then turn180() gotoChest(false, false) return end
    if right.name == chest and behind.name == interface then turtle.turnRight() gotoChest(false, false) return end


    if hasNeighborName(front, behind, left, right, farmland) then
        up(1)
        front, top, under, left, right, behind = getNeighbors()
    elseif hasNeighborName(front, behind, left, right, smoothAEBlock) then
        down(1)
        front, top, under, left, right, behind = getNeighbors()
    end

    --in down tube
    if hasNeighborColor(front, behind, left, right, "black") then
        while not inspectNameColor(stainedGlass, "black") do
            turtle.turnLeft()
        end

        gotoChest(false)
        return
    end

    --in up tube
    if hasNeighborColor(front, behind, left, right, "gray") then
        while not inspectNameColor(stainedGlass, "gray") do
            turtle.turnLeft()
        end

        while not (inspectName(smoothAEBlock) or inspectUpName(slab)) do
            up(1)
        end

        if not inspectUpName(slab) then up(2) end

        gotoChest()
        return
    end

    --above plants
    if (top.name == lantern or top.name == slab or top.name == stone) then
        handlePlantResume(right)
        return
    end

    --in tube change
    if top.name == stainedGlass and (under.name == stainedGlass or under.name == stone) then 
        while not inspectName(stainedGlass) do
            turtle.turnLeft()
        end

        if under.name == stone then 
            turtle.turnLeft()
            forward(1)
            turtle.turnLeft()
            gotoChest(false, false)
        else 
            turn180()
            forward(1)
            gotoChest(false)
        end
    end
end



resume()

while true do
    main()
end
