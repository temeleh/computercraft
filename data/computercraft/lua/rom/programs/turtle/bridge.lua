local args = { ... }
-----------------------------------------------------------------------------------------------------------------------
-- Setup
-----------------------------------------------------------------------------------------------------------------------

--[[
    Executes the movement function stepCount times.
    If movement() returns false then it executes the attack function until movement() returns true
]]
local function simpleMovement(stepCount, movement, attack, dig)
    local c = 1
    while c <= stepCount do
        if movement() then
            c = c + 1
        elseif not dig() then
            attack()
        end
    end
end


local function forward(stepCount) simpleMovement(stepCount, turtle.forward, turtle.attack, turtle.dig) end
local function back(stepCount) simpleMovement(stepCount, turtle.back, turtle.attack, turtle.dig) end

local function left() turtle.turnLeft() end
local function right() turtle.turnRight() end

local function placeBridge()
    local success, data = turtle.inspectDown()

    if success then
        return true
    elseif turtle.getItemCount(1) > 0 then
        return turtle.placeDown()
    else
        for i = 2, 16 do
            if turtle.getItemDetail(i) then
                turtle.select(i) 
                turtle.transferTo(1)
                
                turtle.select(1)
                return turtle.placeDown()
            end
        end
    end
end


for i = 1, tonumber(args[1]) do
    print(i)
    forward(1)
    turtle.digDown()

    if not placeBridge() then
        break
    end
end

