local args = {...}

local protocolName = "door"
local hostName = tostring(args[1])

rednet.open(tostring(args[2]))
rednet.host(protocolName, hostName)

local xRange1 = 0
local xRange2 = 0
local yRange1 = 0
local yRange2 = 0
local zRange1 = 0
local zRange2 = 0

if args[3] ~= nil then xRange1 = tonumber(args[3]) end
if args[4] ~= nil then xRange2 = tonumber(args[4]) end
if args[5] ~= nil then yRange1 = tonumber(args[5]) end
if args[6] ~= nil then yRange2 = tonumber(args[6]) end
if args[5] ~= nil then zRange1 = tonumber(args[7]) end
if args[6] ~= nil then zRange2 = tonumber(args[8]) end

local x, y, z

while true do
    local id, coords = rednet.receive(protocolName)

    x = coords[1]
    y = coords[2]
    z = coords[3]
    
    if x ~= nil and y ~= nil and z ~= nil then
        rs.setOutput("back", x > xRange1 and x < xRange2 and y > yRange1 and y < yRange2 and z > zRange1 and z < zRange2)
    end
end